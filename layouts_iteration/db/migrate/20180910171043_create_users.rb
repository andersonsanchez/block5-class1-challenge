class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :occupation
      t.string :email
      t.string :info
      t.string :imagepath

      t.timestamps
    end
  end
end
